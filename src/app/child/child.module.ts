import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ChildComponent } from './child.component';

@NgModule({
  declarations: [
    ChildComponent,  
  ],
  imports: [
    CommonModule,
  ],
  exports: [ChildComponent],
  providers: [],
})
export class ChildModule { }
