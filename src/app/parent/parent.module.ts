import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ParentComponent } from './parent.component';
import { ChildModule } from '../child/child.module';


@NgModule({
  declarations: [
    ParentComponent,  
  ],
  imports: [
    CommonModule,
    ChildModule,
  ],
  exports: [ParentComponent],
  providers: [],
})
export class ParentModule { }
